@extends('admin/layout/default')
@section('title', 'User-Index')
@section('content')
<div class="row">
 @include ('partials.notifications')   
            <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-info">
                        <div class="panel-heading">
                           Edit Profile
                        </div>
                        <div class="panel-body">
                            <form method="post" action="{{ url('admin/profile/update') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="id" value="{{$user->id}}">
                                        <div class="form-group">
                                            <label>Enter Name</label>
                                            <input class="form-control" type="text" value="{{$user->name}}" name="name" required>
                                            
                                        </div>
                                 <div class="form-group">
                                            <label>Enter Email</label>
                                            <input class="form-control" type="email" value="{{$user->email}}" name="email">
                                     
                                        </div>
                                            <div class="form-group">
                                            <label>Enter PhoneNo</label>
                                            <input class="form-control" type="text" value="{{ $user->phone}}" name="phone">

                                        </div>
                                  
                                 
                                        <button type="submit" class="btn btn-info">UpdateProfile </button>
                                        @if (count($errors->register) > 0)
                                <div class="alert alert-danger">
            <ul>
                @foreach ($errors->register->all() as $error)
                <P>{{ $error }}</p>
                @endforeach
            </ul>
        </div>
                                        @endif

                                    </form>
                            </div>
                        </div>
                            </div>
<div class="col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-danger">
                        <div class="panel-heading">
                           Change Password
                        </div>
                        <div class="panel-body">
                            <form method="post" action="{{ url('admin/changepassword') }}">
                             {!! csrf_field() !!}           
                                 <div class="form-group">
                                            <label>Old Password</label>
                                            <input class="form-control" type="password" name="old_password" value="{{ Input::old('old_password') }}" required>
                                     
                                        </div>
                                            <div class="form-group">
                                            <label>Enter New Password</label>
                                            <input class="form-control" type="password" name="password" placeholder="New Password" required>
                                     
                                        </div>
                                <div class="form-group">
                                            <label>Confirm Password </label>
                                            <input class="form-control" type="password" name="confirm_password" required>
                                     
                                        </div>
                                 
                                        <button type="submit" class="btn btn-danger">Save </button>

                                    </form>
                            </div>
                        </div>
                            </div>
        </div>

@endsection