@extends('admin/layout/default')
@section('title', 'User-Index')
@section('content')



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>

                <div class="col-md-10">

               
                  <!--   Kitchen Sink -->
                    <div class="panel panel-default">
                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th >No</th>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Fathername</th>
                                            <th>Occupation</th>
                                            <th>Address</th>
                                            <th>State</th>
                                            <th>Country</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=0; ?>
                                        @foreach($user as $user)
                                        <tr id="sid{{$user->id}}">
                                            <td><?php $i++ ;?>{{$i}}</td>
                                            <td>{{$user['name']}}</td>
                                            <td>
                                                <img src="{{ asset('public/images/'.$user->filename)}}" height="30px" width="30px">
                                            </td>
                                            <td>{{$user['father_name']}}</td>
                                           
                                            <td>
                                              
                                            {{$user['occupation']}}</td>
                                            <td>{{$user['address']}}</td>
                                            <td>{{$user->country}}</td>
                                            <td>{{$user['state']}}</td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                     <!-- End  Kitchen Sink -->
                </div>
                
            </div>

@endsection
