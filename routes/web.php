<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
#admin login
Route::get('admin/login',['as'=>'admin/login','uses'=>'UserController@adminlogin']);
#admin login
Route::post('admin/login',['as'=>'admin/login','uses'=>'UserController@admindologin']);
#AdminMiddleware
Route::group([ 'prefix' => 'admin', 'middleware' => 'App\Http\Middleware\AdminMiddleware'], function() 
{	
	#Dashboard- - 08/03/2021
	Route::get('index',['as'=>'index','uses'=>'UserController@getindex']);
	#Logout- - 08/03/2021
	Route::get('logout',['as'=>'logout','uses'=>'UserController@getlogout']);	
});

#To display Register form- 07/03/2021
Route::get('/',['as'=>'/','uses'=>'UserController@getform']);

# To add users- - 08/03/2021
Route::post('frontend/user/add',array('as'=>'addUser','uses'=>'UserController@getStore'));

# To display index- - 08/03/2021
Route::any('frontend/user/index',array('as'=>'index','uses'=>'UserController@getIndex'));
#To select states- 07/03/2021
// Route::get('country/{country}/states', 'UserController@getStates');
Route::get('country/{country}/states', 'UserController@getStates');
Route::get('get-state-list','UserController@getStateList');