<?php

namespace App\Http\Controllers;
use App\User;

use App\Country;
use App\Registration;
use App\State;
use Input;
use Validator;
use Redirect;
Use Auth;
Use DB;

use Illuminate\Http\Request;

class UserController extends Controller
{
    #Function to load login page for admin- 08/03/2021
	public function adminlogin()
	{

    	return view('admin.auth.login');
  }
  #Function for adminLogin: - 08/03/2021
  public function admindologin()
  {
   	$input = Input::all();
    $validate = Validator::make(Input::all(), [
          'email' => 'required',
        'password' => 'required'

      ]);
    if (!$validate->fails())
    {
      $userdata = array(
              'email'     => Input::get('email'),
              'password'  => Input::get('password')
          );
          if (Auth::attempt($userdata)) 
          {
            $user = Auth::user();
            
            
              return view('admin.home.index');
              //return redirect()->intended('admin/home/index');
            
                    
          }else{
            return Redirect::back()->with('error', 'Incorrect username or passwords');
          }
      }else{
        return Redirect::back()->with('error', 'Incorrect username or password.');
      }
  }

  #Function to load register page for new one- 07/03/2021
	public function getform()
	{
 		$country =  Country::select('id','country','status','created_at','updated_at')
                    ->get();
        // $state = State::select('id','cid','state','created_at','updated_at')
        //             ->get();,'state' => $state
    
    	return view('frontend.user.register',['country' => $country]);
  }
  #Function to store usres to register table- 07/03/2021
  public function getStore()
  {

    $input=Input::all();
    //$id=Input::get('id');
    if(!empty($input))
    {
      $validate = Validator::make(Input::all(), ['name'  => 'required','father_name' => 'required' ,
         
            ]);
      if(!$validate->fails())

      {
      	// $cid=Input::get('country');
       //  $sid=Input::get('state');
       //  $country =  Country::select('country')
                    
       //      ->where("id",$cid)
       //      ->get();
       //  $state =  State::select('state')
                    
       //      ->where("id",$sid)
       //      ->first();
      	
      		if(Input::hasFile('filename'))
            {
              echo"image";

            
            $file = Input::file('filename') ;
            
            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/images/' ;
            $file->move($destinationPath,$fileName);
            
        }
        	$user     =  new Registration;
            $user->name=Input::get('name');
            $user->father_name   =Input::get('father_name');
            
            $user->occupation   =Input::get('occupation');
            $user->address   =Input::get('address');
            $user->country   =Input::get('country');
            
            $user->state   =nput::get('state');
            $user->filename   =$fileName;;
            $user->status   =1;
            $user->save();

          return Redirect::to('/')->with('success', 'user registered  successfully!');
    }
    
  }

}
#Function to display userindex: - 08/03/2021
  public function getIndex()
  {	
  	$user = Auth::user();
            if($user->status == 0)
            {
              Auth::logout();
              return Redirect::back()->with('error', 'You dont have privilage.');
            }
            else
            {
  			$user =  Registration::select('id','name','father_name','occupation','address','country','state','filename','status','updated_at')
                    ->get();
    		return view('frontend/user/index', ['user' => $user]);
		}
 }
 #select states- 08/03/2021
 public function getStates( $country)
    {
    	
        return $country->states()->select('id', 'state')->get();
    }




    public function getStateList(Request $request)
        {
            $states = DB::table("states")
            ->where("cid",$request->cid)
            ->pluck("state","id");
            return response()->json($states);
            // $states = State::select('id','cid','state','created_at','updated_at')
            // ->where("cid",$request->country_id)
        //             ->get();
        }

}
