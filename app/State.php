<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public function countrynames()
    {
        return $this->belongsTo('app\Country','country');
    }
}
