<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
	protected $table='registrations';
    protected $fillable=['country'];
    public function countryname()
    {
        return $this->belongsTo('app\Country','country');
    }
     public function statename()
    {
        return $this->belongsTo('app\State','state');
    }
}
