<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	protected $table='countries';
    protected $fillable=['country'];
    public function statesname()
    {
        return $this->has_many('State');
    }
    public function register()
    {
        return $this->has_many('Registration');
    }
}
